import loadable from '@loadable/component';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Snackbar from '@material-ui/core/Snackbar';
import { MuiThemeProvider } from '@material-ui/core/styles';
import * as React from 'react';
import { Provider } from 'react-redux';
import 'ui/css/App.css';
import ScreenInfo from 'ui/ScreenInfo';
import About from 'ui/About';
import { Contact } from 'ui/Contact';
import { Hire } from 'ui/Hire';
import { Mission } from 'ui/Mission';
import { Microservices } from 'ui/Microservices';
import { Projects } from 'ui/Projects';
import { Title } from 'ui/Title';
import { Stalk } from 'ui/Stalk';
import { APP_THEME } from 'Config';
import { APP_STORE } from 'store';

// tslint:disable: variable-name
const Footer = loadable(() =>
  import(/* webpackChunkName: "Footer" */ 'ui/Footer')
);

// tslint:disable: variable-name
const AboutDialog = loadable(() =>
  import(/* webpackChunkName: "AboutDialog" */ 'ui/AboutDialog')
);

export class App extends React.Component {
  state = {
    status: '',
  };

  render() {
    const showStatus = this.state.status !== '';
    return (
      <MuiThemeProvider theme={APP_THEME}>
        <CssBaseline />
        <Provider store={APP_STORE}>
          <ScreenInfo />
          <div className="App">
            <div className="btn-group-vertical">
              <Title />

              <Divider />

              <About />
              <AboutDialog />
              <Contact showStatus={this.showStatus} />
              <Hire />
              <Mission />

              <Divider />

              <Microservices />
              <Projects />

              <Divider />

              <Stalk />
            </div>
          </div>
          <Snackbar
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            open={showStatus}
            autoHideDuration={6000}
            onClose={this.hideStatus}
            ContentProps={{
              'aria-describedby': 'status-message',
            }}
            message={<span id="status-message">{this.state.status}</span>}
          />
          <Footer />
        </Provider>
      </MuiThemeProvider>
    );
  }

  private showStatus = (status: string) => {
    this.setState({ status });
  };

  private hideStatus = () => {
    this.setState({ status: '' });
  };
}
