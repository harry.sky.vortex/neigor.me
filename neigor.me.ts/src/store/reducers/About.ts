import { AppState, INITIAL_STATE } from 'store/State';
import { update } from 'store/utils';
import { Actions, AboutAction } from 'store/actions';

export function aboutReducer(
  state = INITIAL_STATE,
  action: AboutAction
): AppState {
  switch (action.type) {
    case Actions.ABOUT_OPEN:
      return update(state, { aboutOpen: true });
    case Actions.ABOUT_CLOSE:
      return update(state, { aboutOpen: false });
    default:
      return state;
  }
}