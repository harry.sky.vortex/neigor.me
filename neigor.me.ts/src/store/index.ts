import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from '@redux-saga/core';
import { screenInfoReducer } from './reducers/ScreenInfo';
import { aboutReducer } from './reducers/About';
import { INITIAL_STATE, AppState } from './State';

const REDUCERS = [
  screenInfoReducer,
  aboutReducer,
];

// tslint:disable-next-line: no-any
function rootReducer(state = INITIAL_STATE, action: any): AppState {
  REDUCERS.forEach(reducer => (state = reducer(state, action)));
  return state;
}

const SAGA = createSagaMiddleware();

export const APP_STORE = createStore(rootReducer, applyMiddleware(SAGA));
