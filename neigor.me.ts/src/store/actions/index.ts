import { ScreenData } from 'DataTypes';

export enum Actions {
  SCREEN_UPDATE = 'SCREEN_UPDATE',
  ABOUT_OPEN = 'ABOUT_OPEN',
  ABOUT_CLOSE = 'ABOUT_CLOSE',
}

export interface ScreenAction {
  type: typeof Actions.SCREEN_UPDATE;
  screen: ScreenData;
}

export type AboutAction =
  | {
      readonly type: typeof Actions.ABOUT_OPEN;
    }
  | {
      readonly type: typeof Actions.ABOUT_CLOSE;
    };
