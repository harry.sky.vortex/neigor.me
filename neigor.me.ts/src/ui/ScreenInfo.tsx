import * as React from 'react';
import { connect } from 'react-redux';
import { Props, mapDispatchToProps } from 'store/ui/ScreenInfo';
import { getScreenData } from 'store/utils';

export class ScreenInfo extends React.PureComponent<Props> {
  render() {
    return false;
  }

  componentDidMount = () => {
    this.onSizeChange();
    window.addEventListener('resize', this.onSizeChange);
  };

  componentWillUnmount = () =>
    window.removeEventListener('resize', this.onSizeChange);

  onSizeChange = () => this.props.update(getScreenData());
}

// tslint:disable-next-line: no-default-export
export default connect(null, mapDispatchToProps)(ScreenInfo);
