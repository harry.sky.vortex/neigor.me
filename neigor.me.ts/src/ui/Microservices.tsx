import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Popover from '@material-ui/core/Popover';
import * as React from 'react';
import 'ui/css/App.css';

export class Microservices extends React.Component {
  state = {
    microservicesAnchorEl: null,
  };

  render() {
    const openMicroservices = Boolean(this.state.microservicesAnchorEl);
    return (
      <div>
        <Button
          onClick={this.openMicroservices}
          variant="contained"
          color="primary"
          fullWidth={true}
        >
          Small/Medium Pet-Projects
        </Button>
        <Popover
          onClose={this.closeMicroservices}
          open={openMicroservices}
          anchorEl={this.state.microservicesAnchorEl}
          anchorOrigin={{
            horizontal: 'center',
            vertical: 'top',
          }}
          transformOrigin={{
            horizontal: 'center',
            vertical: 'bottom',
          }}
        >
          <div className="btn-group-vertical-no-gaps">
            <Button
              color="primary"
              fullWidth={true}
              target="_blank"
              href="https://music.neigor.me/"
              rel="noopener noreferrer"
            >
              Melodiam
            </Button>
            <Button
              color="primary"
              fullWidth={true}
              target="_blank"
              href="https://call.neigor.me/"
              rel="noopener noreferrer"
            >
              Call Notifier
            </Button>
            <Button
              color="primary"
              fullWidth={true}
              target="_blank"
              href="https://ineedyoutohug.me/"
              rel="noopener noreferrer"
            >
              iNeedYouToHug.Me
            </Button>

            <Divider />

            <Button
              color="primary"
              fullWidth={true}
              target="_blank"
              href="https://gitlab.com/harry.sky.vortex/mail"
              rel="noopener noreferrer"
            >
              Mail Server (Docker)
            </Button>
            <Button
              color="primary"
              fullWidth={true}
              target="_blank"
              href="https://gitlab.com/harry.sky.vortex/docker-openvpn"
              rel="noopener noreferrer"
            >
              OpenVPN (Docker)
            </Button>
            <Button
              color="primary"
              fullWidth={true}
              target="_blank"
              href="https://gitlab.com/harry.sky.vortex/docker-devpi"
              rel="noopener noreferrer"
            >
              devpi (Docker)
            </Button>
            <Button
              color="primary"
              fullWidth={true}
              target="_blank"
              href="https://gitlab.com/harry.sky.vortex/docker-devpi"
              rel="noopener noreferrer"
            >
              Ghost Blog (Docker)
            </Button>
          </div>
        </Popover>
      </div>
    );
  }

  private closeMicroservices = () => {
    this.setState({ microservicesAnchorEl: null });
  };

  private openMicroservices = (event: React.MouseEvent<HTMLElement>) => {
    this.setState({
      microservicesAnchorEl: event.currentTarget as HTMLElement,
    });
  };
}
