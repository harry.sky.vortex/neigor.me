import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { APP_THEME } from 'Config';

export class Title extends React.PureComponent {
  render() {
    return (
      <Typography
        component="h4"
        variant="h4"
        gutterBottom={true}
        align="center"
      >
        Not {'{'} <b style={{ color: APP_THEME.palette.primary.main }}>old</b>{' '}
        {'}'} Igor
      </Typography>
    );
  }
}
