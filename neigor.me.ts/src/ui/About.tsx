import Button from '@material-ui/core/Button';
import PersonOutlinedIcon from '@material-ui/icons/PersonOutlined';
import * as React from 'react';
import 'ui/css/App.css';
import { connect } from 'react-redux';
import {
  Props,
  mapDispatchToProps
} from 'store/ui/About';

export class About extends React.PureComponent<Props> {
  render() {
    return (
      <Button
        onClick={this.props.open}
        variant="outlined"
        color="primary"
        fullWidth={true}
      >
        <PersonOutlinedIcon className="left-icon" />
        About
      </Button>
    );
  }
}

// tslint:disable-next-line: no-default-export
export default connect(
  null,
  mapDispatchToProps
)(About);
