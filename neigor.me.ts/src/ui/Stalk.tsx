import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import 'ui/css/App.css';

export class Stalk extends React.PureComponent {
  render() {
    return (
      <React.Fragment>
        <Typography
          component="h6"
          variant="h6"
          gutterBottom={true}
          align="center"
        >
          <s>
            <b style={{ color: 'grey' }}>S</b>
          </s>
          Talk:
        </Typography>
        <ButtonGroup variant="outlined" color="secondary">
          <Button
            target="_blank"
            href="https://vk.com/nehorosev"
            rel="noopener noreferrer"
          >
            VK
          </Button>
          <Button
            target="_blank"
            href="https://instagram.com/hsky.in"
            rel="noopener noreferrer"
          >
            IG
          </Button>
          <Button
            target="_blank"
            href="https://facebook.com/hsky.in"
            rel="noopener noreferrer"
          >
            FB
          </Button>
          <Button
            target="_blank"
            href="https://open.spotify.com/user/harry.sky.vortex"
            rel="noopener noreferrer"
          >
            Spotify
          </Button>
        </ButtonGroup>
        <ButtonGroup variant="outlined" color="primary" className="last-button">
          <Button
            target="_blank"
            href="https://www.linkedin.com/in/igor-nehoroshev/"
            rel="noopener noreferrer"
          >
            LinkedIn
          </Button>
          <Button
            target="_blank"
            href="https://gitlab.com/harry.sky.vortex"
            rel="noopener noreferrer"
          >
            GitLab
          </Button>
          <Button
            target="_blank"
            href="https://github.com/HarrySky"
            rel="noopener noreferrer"
          >
            GitHub
          </Button>
        </ButtonGroup>
      </React.Fragment>
    );
  }
}
