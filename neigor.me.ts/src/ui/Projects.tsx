import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Popover from '@material-ui/core/Popover';
import * as React from 'react';
import 'ui/css/App.css';

export class Projects extends React.Component {
  state = {
    projectsAnchorEl: null,
  };

  render() {
    const openProjects = Boolean(this.state.projectsAnchorEl);
    return (
      <div>
        <Button
          onClick={this.openProjects}
          variant="contained"
          color="secondary"
          fullWidth={true}
        >
          More Serious Projects
        </Button>
        <Popover
          onClose={this.closeProjects}
          open={openProjects}
          anchorEl={this.state.projectsAnchorEl}
          anchorOrigin={{
            horizontal: 'center',
            vertical: 'top',
          }}
          transformOrigin={{
            horizontal: 'center',
            vertical: 'bottom',
          }}
        >
          <div className="btn-group-vertical-no-gaps">
            <Button
              color="secondary"
              fullWidth={true}
              target="_blank"
              href="https://blog.neigor.me/worldman-games/"
              rel="noopener noreferrer"
            >
              Worldman Games (Tech Lead)
            </Button>
            <Button
              color="secondary"
              fullWidth={true}
              target="_blank"
              href="https://cryptime.ee"
              rel="noopener noreferrer"
            >
              Cryptime (Tech Lead / CTO)
            </Button>

            <Divider />

            <Button
              color="secondary"
              fullWidth={true}
              target="_blank"
              href="https://pypi.org/project/starlette-early-data/"
              rel="noopener noreferrer"
            >
              starlette-early-data (Middleware)
            </Button>
            <Button
              color="secondary"
              fullWidth={true}
              target="_blank"
              href="https://pypi.org/project/webserver/"
              rel="noopener noreferrer"
            >
              webserver (CLI tool)
            </Button>
          </div>
        </Popover>
      </div>
    );
  }

  private closeProjects = () => {
    this.setState({ projectsAnchorEl: null });
  };

  private openProjects = (event: React.MouseEvent<HTMLElement>) => {
    this.setState({ projectsAnchorEl: event.currentTarget as HTMLElement });
  };
}
