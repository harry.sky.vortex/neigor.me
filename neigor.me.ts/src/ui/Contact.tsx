import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import EmailIcon from '@material-ui/icons/Email';
import * as React from 'react';
import 'ui/css/App.css';

interface ContactProps {
  showStatus(status: string): void;
}

export class Contact extends React.Component<ContactProps> {
  state = {
    contactOpen: false,
    email: '',
    message: '',
    name: '',
  };

  render() {
    return (
      <React.Fragment>
        <Button
          onClick={this.openContact}
          variant="outlined"
          color="secondary"
          fullWidth={true}
        >
          <EmailIcon className="left-icon" />
          Contact
        </Button>
        <Dialog
          open={this.state.contactOpen}
          onClose={this.closeContact}
          aria-labelledby="about-me-title"
        >
          <DialogTitle id="about-me-title">Quick Contact Form</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Contact me <i>quickly</i> via form or use email below
              <br />
              <br />
              Talk:
              <br />
              <b>mail</b> [at] <b>neigor.me</b>
              <br />
              Business:
              <br />
              <b>harry</b> [at] <b>hashdivision.com</b>
            </DialogContentText>
            <TextField
              margin="none"
              label="Your Name"
              type="text"
              fullWidth={true}
              required={true}
              value={this.state.name}
              onChange={this.handleNameChange}
            />
            <TextField
              margin="none"
              label="E-Mail"
              type="email"
              fullWidth={true}
              required={true}
              value={this.state.email}
              onChange={this.handleEmailChange}
            />
            <TextField
              margin="none"
              label="Message (max 300)"
              type="text"
              multiline={true}
              rowsMax="4"
              fullWidth={true}
              required={true}
              value={this.state.message}
              onChange={this.handleMessageChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeContact} color="secondary">
              Cancel
            </Button>
            <Button onClick={this.sendMessage} color="primary">
              Send Message
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }

  private closeContact = () => {
    this.setState({ contactOpen: false, name: '', email: '', message: '' });
  };

  private openContact = () => {
    this.setState({ contactOpen: true, name: '', email: '', message: '' });
  };

  private sendMessage = () => {
    fetch('/api/message', {
      body:
        'name=' +
        encodeURIComponent(this.state.name) +
        '&email=' +
        encodeURIComponent(this.state.email) +
        '&message=' +
        encodeURIComponent(this.state.message),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
    })
      .then(response => {
        if (response.status === 429) {
          return 'Too Many Requests';
        }
        return response.text();
      })
      .then(responseText => {
        this.props.showStatus(responseText);
        if (responseText === 'Message Sent!') {
          this.closeContact();
        }
      });
  };

  private handleNameChange = (evt: React.ChangeEvent) => {
    this.setState({ name: (evt.target as TextFieldProps).value as string });
  };

  private handleEmailChange = (evt: React.ChangeEvent) => {
    this.setState({ email: (evt.target as TextFieldProps).value as string });
  };

  private handleMessageChange = (evt: React.ChangeEvent) => {
    let message = (evt.target as TextFieldProps).value as string;
    if (message.length > 300) {
      message = message.substring(0, 300);
      (evt.target as TextFieldProps).value = message;
    }

    this.setState({ message });
  };
}
