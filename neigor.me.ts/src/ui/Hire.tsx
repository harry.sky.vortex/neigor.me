import Button from '@material-ui/core/Button';
import AssignmentIcon from '@material-ui/icons/Assignment';
import * as React from 'react';
import 'ui/css/App.css';

export class Hire extends React.PureComponent {
  render() {
    return (
      <Button
        variant="outlined"
        color="secondary"
        fullWidth={true}
        target="_blank"
        href="https://igornehoroshev.com/"
        rel="noopener noreferrer"
      >
        <AssignmentIcon className="left-icon" />
        CV / Hire
      </Button>
    );
  }
}
