from os import getenv
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.requests import Request as StarletteRequest
from starlette import status
from aiosmtplib import SMTP

SMTP_HOST = getenv('SMTP_HOST', 'localhost')
SMTP_PORT = int(getenv('SMTP_PORT', '25'))

SERVER = Starlette()

MESSAGE_SENT = PlainTextResponse('Message Sent!')
BAD_REQUEST = PlainTextResponse('Bad Request!', status_code=status.HTTP_400_BAD_REQUEST)
MAIL_SERVER_ERROR = PlainTextResponse('Mail Server Problem!', status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

@SERVER.route('/message', methods=['POST'])
async def message_endpoint(request: StarletteRequest) -> PlainTextResponse:
    form = await request.form()
    if 'name' not in form or 'email' not in form or 'message' not in form:
        return BAD_REQUEST

    name = form['name']
    email = form['email']
    message = form['message']
    if len(name) == 0 or len(name) > 64 or len(email) == 0 or len(email) > 64 or len(message) == 0 or len(message) > 300:
        return BAD_REQUEST

    mail_server = SMTP(hostname=SMTP_HOST, port=SMTP_PORT)
    mail_message = MIMEMultipart('alternative')
    mail_message.attach(MIMEText(f"""New quick contact form message from {name} <{email}>

{message}""", 'plain', 'utf-8'))
    mail_message.attach(MIMEText(f"""<html>
        <head>
            <title>Contact Form Message</title>
        </head>
        <body>
            <h3>New quick contact form message from {name} ({email})</h3>
            <pre>{message}</pre>
        <body>
    </html>""", 'html', 'utf-8'))
    mail_message['From'] = getenv('FROM', 'Send Er <sender@example.com>')
    mail_message['To'] = getenv('TO', 'Receiv Er <receiver@example.com>')
    mail_message['Subject'] = getenv('SUBJECT', 'Test Subject')
    try:
        await mail_server.connect()
        await mail_server.send_message(mail_message)
        await mail_server.quit()
    except:
        return MAIL_SERVER_ERROR

    return MESSAGE_SENT
